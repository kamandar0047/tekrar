package com.muro.az.demo2.config;

import com.muro.az.demo2.domain.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Config {

    @Bean(name = "a")
    public A instance() {
        return new A();
    }


    @Bean
    public C instance1() {
        return new C();
    }

    @Bean(name = "v1")
    public Vinterface instance4() {
        return new VinImpl1();
    }

    @Bean(name = "v2")

    public Vinterface instance5() {
        return new VinImpl1();
    }
}
