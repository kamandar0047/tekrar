package com.muro.az.demo2.domain;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class B {

private final  A a;

public void help(){
    a.test();
}

}
