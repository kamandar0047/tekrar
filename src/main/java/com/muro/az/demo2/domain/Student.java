package com.muro.az.demo2.domain;


import javax.persistence.*;

@Entity
@Table(name = "students")
public class Student {
    @Id
   @GeneratedValue
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    private String name;
  private Integer age;
  private String homeAddress;
  private String school;
  private String description;
  private Long grade;
}
