package com.muro.az.demo2.repository;

import com.muro.az.demo2.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {



}
